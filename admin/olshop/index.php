<?php

	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	if( !isset( $_SESSION['login'] ) or $_SESSION['login'] == false ) {
		header("location:../login.php");
	}
	
?>

<!DOCTYPE html>
	<html>
		<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="../../lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="../../lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../../lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="../../lib/css/style.css" rel="stylesheet">
		<link href="../../lib/css/reset.css" rel="stylesheet">
		<link href="../../lib/css/stylecadangan.css" rel="stylesheet">

		</head>
		<body>
		<?php getHeader(); ?>

		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend>Member Name</legend>
							<ul class="sidebar user-sidebar">
								<a href="../"><li class="border-neutral">Home</li></a>
								<a href="../member/"><li class="border-neutral">Manage Member</li></a>
								<a href="../olshop/"><li class="border-neutral active">Manage Online Shop</li></a>
								<a href="../kategori/"><li class="border-neutral">Manage Category</li></a>
								<a href="../product"><li class="border-neutral">Manage Product</li></a>
								<a href="../cart"><li class="border-neutral">Manage Shopping Cart</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
					<h4 class="content-table-head">Manage online shop</h4>
					<p class="content-table-description">Manage your online shop you have</p>
					<div class="separator"></div>
					<?php
					if( isset( $_GET['alert'] ) ) {
						if( $_GET['alert'] == "editfailed" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to update data, please try again in few moment. Click on me to dismiss.</div>";
						} else if( $_GET['alert'] == "editsuccess" ) {
							echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Succesfully Updated!</strong> Your data successfully updated. Click on me to dismiss.</div>";
						} else if( $_GET['alert'] == "addfailed" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to add data, please try again in few moment. Click on me to dismiss.</div>";
						} else if( $_GET['alert'] == "addsuccess" ) {
							echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Succesfully Added!</strong> Your data successfully added. Click on me to dismiss.</div>";
						} else if( $_GET['alert'] == "deletefailed" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to delete data, please try again in few moment. Click on me to dismiss.</div>";
						} else if( $_GET['alert'] == "deletesuccess" ) {
							echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Succesfully Deleted!</strong> Your data successfully deleted. Click on me to dismiss.</div>";
						}
					}
					if(isset($_GET['mode'])){
						if( $_GET['mode']=="add") {
							require_once("add.php");
						} else if( $_GET['mode']=="edit" ) {
							require_once("edit.php");
						} else if( $_GET['mode']=="delete" ) {
							require_once("../function/olshop/delete.php");
						}
					} else {
						require_once("../function/olshop/list.php");
					}
					?>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="../../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../../lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../../lib/js/custom.js"></script>
		<script src="../../lib/js/buy.js"></script>

		</body>
	</html>



