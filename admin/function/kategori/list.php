<?php

// Getting data from database
$query = mysql_query("SELECT * FROM kategori");

// Show data from database
?>
<p class="note">Note : Do not edit this table if you don't know the implications.</p>
<table class="list-content">
	<thead>
		<tr class="table-line">
			<th class="table-head">ID Category</th>
			<th class="table-head">Category</th>
			<th class="table-head">Action</th>
		</tr>
	</thead>
	<tbody>
<?php
if( mysql_num_rows($query) == "0" ){
	echo "<tr class=\"table-line\"><td class=\"table-content\" colspan=\"4\">No category available. Go create one.</td></tr>";
} else {
	while( $row = mysql_fetch_array($query) ) {
		// Copy data from database to variable(s)
		$idKategori = $row['0'];
		$kategori = $row['1'];

		// Show data with table
		echo "<tr class=\"table-line\">";
		echo "<td class=\"table-content\">" . $idKategori     . "</td>";
		echo "<td class=\"table-content\">". $kategori   . "</td>";
		echo "<td class=\"table-content\"><a href=\"index.php?mode=edit&idKategori=" . $idKategori . "\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>&nbsp;<a href=\"index.php?mode=delete&idKategori=" . $idKategori . "\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a></td>";
		echo "</tr>";
	}
}

?>
	</tbody>
	<tfoot>
		<tr class="table-line">
			<td colspan="3" class="table-foot"><a href="index.php?mode=add"><span class="glyphicon glyphicon-plus"></span> Add new category</a></td>
		</tr>
	</tfoot>
</table>