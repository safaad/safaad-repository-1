<?php

// Getting data from database
$query = mysql_query("SELECT * FROM member");

// Show data from database
?>
<p class="note">Note : Do not edit this table if you don't know the implications.</p>
<table class="list-content">
	<thead>
		<tr class="table-line">
			<th class="table-head">ID Member<br />Position</th>
			<th class="table-head">Name</th>
			<th class="table-head">E-Mail</th>
			<th class="table-head">Phone Number</th>
			<th class="table-head">Action</th>
		</tr>
	</thead>
	<tbody>
<?php
if( mysql_num_rows($query) == "0" ){
	echo "<tr class=\"table-line\"><td class=\"table-content\" colspan=\"4\">No member available. Go create one.</td></tr>";
} else {
	while( $row = mysql_fetch_array($query) ) {
		// Copy data from database to variable(s)
		$idMember 	= $row['0'];
		$idPosition = $row['1'];
		$namaMember = $row['2'];
		$email 		= $row['3'];
		$noHP		= $row['4'];

		// Copy position data from database to variable
		$q_position = mysql_query("SELECT POSITIONNAME FROM position WHERE IDPOSITION='$idPosition'");
		if( $row = mysql_fetch_array($q_position) ) {
			$position = $row['0'];
		}

		// Show data with table
		echo "<tr class=\"table-line\">";
		echo "<td class=\"table-content\">" . $idMember . "<br />" . $position . "</td>";
		echo "<td class=\"table-content\">". $namaMember  . "</td>";
		echo "<td class=\"table-content\">" . $email . "</td>";
		echo "<td class=\"table-content\">" . $noHP . "</td>";
		echo "<td class=\"table-content\"><a href=\"index.php?mode=edit&idMember=" . $idMember . "\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>&nbsp;<a href=\"index.php?mode=delete&idMember=" . $idMember . "\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a></td>";
		echo "</tr>";
	}
}

?>
	</tbody>
	<tfoot>
		<tr class="table-line">
			<td colspan="5" class="table-foot"><a href="index.php?mode=add"><span class="glyphicon glyphicon-plus"></span> Add new category</a></td>
		</tr>
	</tfoot>
</table>