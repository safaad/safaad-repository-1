<?php

// Getting data from database
$query = mysql_query("SELECT * FROM produk");

// Show data from database
?>
<p class="note">Note : Do not edit this table if you don't know the implications.</p>
<table class="list-content">
	<thead>
		<tr class="table-line">
			<th class="table-head">ID Product<br />ID Shop</th>
			<th class="table-head">Category</th>
			<th class="table-head">Product Name</th>
			<th class="table-head">Price / Stock</th>
			<th class="table-head">Action</th>
		</tr>
	</thead>
	<tbody>
<?php
if( mysql_num_rows($query) == "0" ){
	echo "<tr class=\"table-line\"><td class=\"table-content\" colspan=\"4\">No category available. Go create one.</td></tr>";
} else {
	while( $row = mysql_fetch_array($query) ) {
		// Copy data from database to variable(s)
		$idProduct 		= $row['0'];
		$idKategori 	= $row['1'];
		$idToko 		= $row['2'];
		$namaProduct 	= $row['3'];
		$harga 			= $row['6'];
		$stok 			= $row['7'];

		// Show data with table
		echo "<tr class=\"table-line\">";
		$q_toko = mysql_query("SELECT NAMATOKO FROM toko WHERE IDTOKO = '$idToko'");
		$s_toko = mysql_fetch_array( $q_toko );
		echo "<td class=\"table-content\">" . $idProduct . "<br />" . $s_toko['0'] . "</td>";
		$q_kategori = mysql_query("SELECT KATEGORINAME FROM kategori WHERE IDKATEGORI='$idKategori'");
		$s_kategori = mysql_fetch_array($q_kategori);
		echo "<td class=\"table-content\">". $s_kategori['0'] . "</td>";
		echo "<td class=\"table-content\">" . $namaProduct . "</td>";
		echo "<td class=\"table-content\">" . $harga . " / " . $stok . "</td>";
		echo "<td class=\"table-content\"><a href=\"index.php?mode=edit&idKategori=" . $idProduct . "\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>&nbsp;<a href=\"index.php?mode=delete&idKategori=" . $idProduct . "\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a></td>";
		echo "</tr>";
	}
}

?>
	</tbody>
	<tfoot>
		<tr class="table-line">
			<td colspan="5" class="table-foot"><a href="index.php?mode=add"><span class="glyphicon glyphicon-plus"></span> Add new category</a></td>
		</tr>
	</tfoot>
</table>