<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	if( !isset( $_SESSION['login'] ) or $_SESSION['login'] == false ) {
		header("location:" . $_SERVER['DOCUMENT_ROOT'] . "/webMagang/login.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="../../lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="../../lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../../lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="../../lib/css/style.css" rel="stylesheet">
		<link href="../../lib/css/reset.css" rel="stylesheet">
		<link href="../../lib/css/stylecadangan.css" rel="stylesheet">

	</head>
	<body>
		<?php getHeader(); ?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend>Setting</legend>
							<ul class="sidebar user-sidebar">
								<a href="../index.php"><li class="border-neutral ">Umum</li></a>
								<a href="riwayat"><li class="border-neutral active">Riwayat Transaksi</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
						<div class="transaksi six">
							<div class="header-setting">
							<h4>Riwayat Transaksi</h4>
								<div>
									<table class="ri-tran">
										<thead>
											<tr>
												<th>Nama Barang</th><th>Jumlah</th><th>Harga Satuan</th><th>Sub Total</th><th>Alamat Penerima</th><th>Tanggal</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$uId = $_SESSION['idMember'];
											$query = mysql_query("SELECT pembelian.IDPEMBELIAN, pembelian.IDPRODUK, produk.NAMAPRODUK, produk.HARGA, pembelian.STOKPEMBELIAN, pembelian.ALAMATPENERIMA FROM pembelian, produk WHERE pembelian.IDPRODUK = produk.IDPRODUK");
											if ($row = mysql_fetch_array($query)) {
											?>
												<tr>
													<td><?php echo $row['2'] ?></td>
													<td><?php echo $row['4']; ?></td>
													<td><?php echo $row['3']; ?></td>
													<td>
														<?php
															$harga = $row['3'];
															$jum = $row['4'];
															$subtotal = $harga * $jum;

															echo $subtotal;
														?>
													</td>
													<td><?php echo $row['5']; ?></td>
													<td></td>
												</tr>
											<?php
											}
										?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="../../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../../lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../../lib/js/custom.js"></script>

	</body>
</html>