<!-- IT'S A SPECIAL PAGE -->
<?php

require_once("function/router.php");
require_once("function/function.php");

?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<meta name="description" content="">
			<meta name="author" content="Safaad">

			<title>Login | Safaad</title>

			<!-- Bootstrap core CSS -->
			<link rel="stylesheet" href="lib/css/bootstrap.css">
			<!-- Thing for Social Button ( Font Awesome ) -->
			<link rel="stylesheet" href="lib/css/font-awesome.css">
			<!-- Custom CSS -->
			<link rel="stylesheet" href="lib/css/style.css">
			<link rel="stylesheet" href="lib/css/stylecadangan.css">
			<link rel="stylesheet" href="lib/css/reset.css">
		</head>
		<body>
			<header class="navbarheader">
				<div class="container">
					<div id="col">
						<div class="col col-20">
							<a href="./"><img class="logo" src="lib/img/logo-backend1.png"></a>
						</div>
						<div class="col col-80">&nbsp;</div>
					</div>
				</div>
			</header>
			<div class="container" style="margin-top: 5px; background-color: white; border: 1px solid gray;">
				<div class="row" style="padding: 25px;">
					<div class="col-md-8">
					<h2>Join Us</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrus exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
					<div class="col-md-4">
					<h2>Login</h2>
					<form action="" method="POST">
						<label for="email">E-Mail</label><br />
						<input class="form-control" type="email" name="email" required><br />
						<label for="password">Password</label><br />
						<input class="form-control" type="password" name="password" required><br />
						<input class="btn btn-default" type="submit" name="submit" value="Login"><br />
						<hr style="border-color: lightgray">
						<a href="#">Forgot password</a> or <a href="register.php">Don't have any account?</a>
					</form>
					<div class="separator"></div>
					<h3>Supported payment methods</h3>
					<div class="row">
						<div class="col-md-3" style="padding: 0">
						<img src="lib/img/bank_mega.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-3" style="padding: 0">
						<img src="lib/img/bank_bni.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-3" style="padding: 0">
						<img src="lib/img/bank_bri.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-3" style="padding: 0">
						<img src="lib/img/bank_btn.jpg" style="width: 100%; height: auto">
						</div>
					</div>
					</div>
				</div>
			</div>
			<footer>
				<div class="container">
					<div id="col">
						<div class="col col-50">
							@Copyright 2016. All Right Reserved. 
						</div>
						<div class="col col-50">
						</div>
					</div>
				</div>
			</footer>
		</body>
	</html>

<?php

if ( isset( $_POST['submit'] ) ) {
	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$q_login = mysql_query("SELECT * FROM member WHERE EMAIL = '$email' AND KATASANDI = '$password'");
	$c_login = mysql_num_rows( $q_login );
	if ( $c_login == 1 and $s_login = mysql_fetch_array( $q_login ) ) {
		$_SESSION['login'] = true;
		$_SESSION['position'] = $s_login['1'];
		$_SESSION['idMember'] = $s_login['0'];

		echo "<meta http-equiv='refresh' content='0;url=./'>";
	}
}

?>