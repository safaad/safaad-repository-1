<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="lib/css/style.css" rel="stylesheet">
		<link href="lib/css/reset.css" rel="stylesheet">
		
	</head>
	<body>
		<?php getHeader(); ?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend align="center">Kategori</legend>
							<ul class="sidebar">
								<a href="category.php?idkategori=1"><li class="border-aksesoris-fashion">Aksesoris & Fashion</li></a>
								<a href="category.php?idkategori=2"><li class="border-elektronik">Elektronik</li></a>
								<a href="category.php?idkategori=3"><li class="border-hobi">Hobi</li></a>
								<a href="category.php?idkategori=4"><li class="border-office-stationery">Office & Stationery</li></a>
								<a href="category.php?idkategori=5"><li class="border-olahraga">Olahraga</li></a>
								<a href="category.php?idkategori=6"><li class="border-otomotif">Otomotif</li></a>
								<a href="category.php?idkategori=7"><li class="border-rumahtangga">Rumah Tangga</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
							<?php
								if ( !isset( $_POST['submit-search'] ) ) {
									$idKategori = $_GET['idkategori'];
									$q_kategori = mysql_query("SELECT * FROM kategori WHERE IDKATEGORI='$idKategori'");
									$s_kategori = mysql_fetch_array( $q_kategori );
									$title 		= $s_kategori['KATEGORINAME'];
									$q_produk 	= mysql_query("SELECT * FROM produk WHERE IDKATEGORI='$idKategori'");
								} else {
									$search = $_POST['search-form'];
									$title 		= "Product found : ";
									$q_produk 	= mysql_query("SELECT * FROM produk WHERE NAMAPRODUK LIKE '%$search%' OR HARGA LIKE '%$search%' OR DESKRIPSI LIKE '%$search%'");
								}
							?>
								<div class="recommendation">
									<fieldset>
										<legend class="recommendation-title"><?php echo $title; ?></legend>
										<div id="col">
										<?php

										if ( mysql_num_rows( $q_produk ) != 0 ) {
											$count=1;
											while( $s_produk = mysql_fetch_array( $q_produk ) ) {
												echo "<div class=\"col col-25\">";
												echo "<a href=\"details.php?idproduk=" . $s_produk['IDPRODUK'] . "\">";
												echo "<div class=\"recommendation-image\">";
												require("function/img.php");
												echo "</div>";
												echo "<div class=\"recommendation-detail\">";
												echo "<span class=\"name\">" . $s_produk['NAMAPRODUK'] . "</span>";
												echo "<span class=\"price\">Rp. " . number_format( $s_produk['HARGA'],2,",",".") . "</span>";
												echo "</div>";
												echo "</a>";
												echo "</div>";
												if( $count == 16 ) {break;}
												$count = $count + 1;
											}
										?>
													</div>
													<div class="col col-100">
														<div class="button-body">
															<button type="submit">Previous</button>
															<button type="submit"><a href="page1.php">1</a></button>
															<button type="submit"><a href="page2.php">2</a></button>
															<button type="submit"><a href="page3.php">3</a></button>
															<button type="submit"><a href="page4.php">4</a></button>
															<button type="submit"><a href="page5.php">5</a></button>
															<button type="submit">Next</button>
														</div>
													</div>
							<?php
									} else echo "No product found!.";
							?>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>
	
	<!-- Bootstrap core JavaScript -->
	<!-- =============================================================== -->
	<script scr="lib/js/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="lib/js/jquery.min.js"><\/script>')</script>
	<script src="lib/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="lib/js/ie10-viewport-bug-workaround.js"></script>
	<script src="lib/js/custom.js"></script>
	
	</body>
</html>