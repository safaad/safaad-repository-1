<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="lib/css/style.css" rel="stylesheet">
		<link href="lib/css/reset.css" rel="stylesheet">
	</head>
	<body>
		<?php getHeader(); ?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend align="center">Pengaturan</legend>
							<ul>
								<li class="border-setting umum active">Umum</li>
								<li class="border-setting privasi">Privasi</li>
								<li class="border-setting pembelian">Pembelian</li>
								<li class="border-setting notifikasi">Notifikasi</li>
								<li class="border-setting online">Toko Online</li>
								<li class="border-setting riwayat">Riwayat Transaksi</li>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
						<div class="recommendation">
							<div class="all-setting">
								<div class="one">
									<div class="header-setting">
										<h4>Pengaturan Umum Akun</h4>
									</div>
									<div class="all-settings">
										<table class="table-sett">
											<tr class="optional tr-table name">
												<td>
													<span class="full-set namess">Nama</span>
													<span class="sunting namess"> Sunting</span>
												</td>
											</tr>
											<tr class="tr-table">
												<td>
													<span class="full-set passwords">Password</span>
													<span class="sunting passwords">Sunting</span>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="lib/js/jquery.min.js"><\/script>')</script>
		<script src="lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="lib/js/custom.js"></script>

	</body>
</html>