<?php
	include($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	include($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");
	
	if( !isset( $_SESSION['login'] ) or $_SESSION['login'] == false ) {
		header("location:../login.php");
	}

	if( isset( $_GET['idtoko'] ) ) {
		$_SESSION['idToko']=$_GET['idtoko'];
		$firstin = true;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="../lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="../lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="../lib/css/style.css" rel="stylesheet">
		<link href="../lib/css/reset.css" rel="stylesheet">
		<link href="../lib/css/stylecadangan.css" rel="stylesheet">

	</head>
	<body>
		<?php getHeader(); ?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend>Shop Panel</legend>
							<ul class="sidebar user-sidebar">
								<a href="./"><li class="border-neutral active">Overall</li></a>
								<a href="offer/"><li class="border-neutral">Offer</li></a>
								<a href="setting/"><li class="border-neutral">Setting</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
					<?php
						if( !isset( $_SESSION['idToko'] ) ) {
							echo "<div class=\"alert alert-warning\"><strong>Warning!</strong> You are not suppose to be here. We will redirect you in 1 second.</div>";
							echo "<meta http-equiv='refresh' content='1;url=../'";
						} else if ( isset( $firstin ) ) {
							echo "<div class=\"alert alert-info\"><strong>Info!</strong> View your bussiness here. Sell product, manage, or make an incredible discount. Click on me to dismiss</div>";
						}
						include("function/dashboard/newproduct.php");
					?>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../lib/js/custom.js"></script>
		<script src="../lib/js/buy.js"></script>
	</body>
</html>