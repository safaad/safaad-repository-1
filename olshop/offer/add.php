<?php	
if( isset( $_SESSION['idToko'] ) ) {
	if( isset( $_GET['mode'] ) ) {
		if( $_GET['mode'] == "add") {
			?>
			<form action="../function/offer/add.php" method="POST" enctype="multipart/form-data" style="padding: 15px">
				<label for="idProdukToko">ID Produk / ID Toko</label><br/>
				<input class="form-control" type="text" name="idProdukToko" value="Auto generate / <?php echo $_SESSION['idToko']; ?>" readonly><br/>
				<label for="namaProduk">Nama Produk</label><br/>
				<input class="form-control" type="text" name="namaProduk" required><br/>
				<label for="kategori">Kategori</label><br />
				<select class="form-control" name="kategori">
			<?php
				$k_query = mysql_query("SELECT * FROM kategori");
				$ck_query = mysql_num_rows( $k_query );
				if( $ck_query == "0" ) {
					echo "<option>No category available</option>";
				} else {
					while( $row = mysql_fetch_array( $k_query ) ) {
						echo "<option value=\"" . $row['0'] . "\">" . $row['1'] . "</option>";
					}
				}
			?>
				</select>
				<label for="harga">Harga</label><br />
				<input class="form-control" type="money" name="harga" required><br />
				<label for="stok">Stok</label><br />
				<input class="form-control" type="number" name="stok" required><br />
				<label for="deskripsi">Deskripsi</label><br/>
				<textarea class="form-control" name="deskripsi"></textarea><br/>
				<label for="gambar">Gambar Produk</label><br />
				<input type="file" name="file"><br />
				<input class="btn btn-default" type="submit" name="submit" value="Submit">&nbsp;<input class="btn btn-default" type="reset" name="reset" value"Reset">
			</form>
			<?php
		} else {?>
			<script>alert(\"You are not in add mode. We will redirecting you to last page.");</script>
			<meta http-equiv='refresh' content='0;url=index.php'>
		<?php
		}
	} else {
		echo "<script>alert(\"Sorry, the data is currently unavailable. Please try again in few moment.\");";
		echo "<meta http-equiv='refresh' content='0;url=index.php'>";
	}
} else {
?>

<script>alert("Sorry, we can't proceed your account. Please retry login.");</script>
<meta http-equiv="refresh" content="0;url=index.php">

<?php	
}

?>