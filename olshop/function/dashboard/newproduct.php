<p><strong>Laporan penawaran produk :</strong></p>

<?php
// Menampilkan penawaran produk perhari, perminggu, perbulan
$idToko = $_SESSION['idToko'];
$hari = date('Y-m-d');
$minggu = date('Y-m-d', strtotime("-1 week"));
$bulan = date('Y-m-d', strtotime("-1 month"));
$q_produkHarian		= mysql_query("SELECT * FROM produk WHERE IDTOKO = '$idToko' AND SUBSTRING(TANGGALPENAWARAN,1,11) = '$hari'") or die(mysql_error());
$q_produkMingguan	= mysql_query("SELECT * FROM produk WHERE IDTOKO = '$idToko' AND SUBSTRING(TANGGALPENAWARAN,1,11) BETWEEN '$minggu' AND '$hari'") or die(mysql_error());
$q_produkBulanan	= mysql_query("SELECT * FROM produk WHERE IDTOKO = '$idToko' AND SUBSTRING(TANGGALPENAWARAN,1,11) BETWEEN '$bulan' AND '$hari'") or die(mysql_error());
?>

<table>
	<tr>
		<th colspan="3">Penawaran Produk</th>
	</tr>
	<tr>
		<th>Harian</th><th>Mingguan</th><th>Bulanan</th>
	</tr>
	<?php
		echo "<tr align=\"center\">";
		echo "<td>" . mysql_num_rows( $q_produkHarian ) . "</td>";
		echo "<td>" . mysql_num_rows( $q_produkMingguan ) . "</td>";
		echo "<td>" . mysql_num_rows( $q_produkBulanan ) . "</td>";
		echo "</tr>";
	?>
</table>
<br />
<p><strong>Laporan penjualan produk :</strong></p>

<?php
// Menampilkan penjualan produk perhari, perminggu, perbulan
$q_produk = mysql_query("SELECT IDPRODUK,NAMAPRODUK FROM produk WHERE IDTOKO = '$idToko'") or die(mysql_error());

?>

<table>
	<tr>
		<th rowspan="2">Nama Produk</th><th colspan="3">Penjualan</th>
	</tr>
	<tr>
		<th>Harian</th><th>Mingguan</th><th>Bulanan</th>
	</tr>
	<?php
	while ( $s_produk = mysql_fetch_array( $q_produk ) ) {
		$idProduk = $s_produk['IDPRODUK'];
		$q_pembelianHarian		= mysql_query("SELECT STOKPEMBELIAN FROM pembelian WHERE IDPRODUK = '$idProduk' AND SUBSTRING(TANGGALPEMBELIAN,1,11) = '$hari'") or die(mysql_error());
		$q_pembelianMingguan	= mysql_query("SELECT STOKPEMBELIAN FROM pembelian WHERE IDPRODUK = '$idProduk' AND SUBSTRING(TANGGALPEMBELIAN,1,11) BETWEEN '$minggu' AND '$hari'") or die(mysql_error());
		$q_pembelianBulanan		= mysql_query("SELECT STOKPEMBELIAN FROM pembelian WHERE IDPRODUK = '$idProduk' AND SUBSTRING(TANGGALPEMBELIAN,1,11) BETWEEN '$bulan' AND '$hari'") or die(mysql_error());
		$stokPembelianHarian	= 0;
		$stokPembelianMingguan	= 0;
		$stokPembelianBulanan	= 0;
		while ( $s_pembelian = mysql_fetch_array($q_pembelianHarian) ) {
			$stokPembelianHarian += $s_pembelian['STOKPEMBELIAN'];
		}
		while ( $s_pembelian = mysql_fetch_array($q_pembelianMingguan) ) {
			$stokPembelianMingguan += $s_pembelian['STOKPEMBELIAN'];
		}
		while ( $s_pembelian = mysql_fetch_array($q_pembelianBulanan) ) {
			$stokPembelianBulanan += $s_pembelian['STOKPEMBELIAN'];
		}
		echo "<tr>";
		echo "<td>" . $s_produk['NAMAPRODUK'] . "</td>";
		echo "<td align=\"center\">" . $stokPembelianHarian . "</td>";
		echo "<td align=\"center\">" . $stokPembelianMingguan . "</td>";
		echo "<td align=\"center\">" . $stokPembelianBulanan . "</td>";
		echo "</tr>";
	}
	
	
	?>
</table>