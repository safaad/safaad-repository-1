<?php

// Getting shop id from session
$uId = $_SESSION['idToko'];

// Getting data from database
$query = mysql_query("SELECT p.IDPRODUK,p.NAMAPRODUK,p.FOTOPRODUK,p.HARGA,p.STOK,k.KATEGORINAME FROM produk p, kategori k WHERE p.IDTOKO = '$uId' and k.IDKATEGORI=p.IDKATEGORI") or die(mysql_error());

// Show data from database
?>
<p class="note">Note : Click on one of Shop's Name to display additional features.</p>
<table class="list-content">
	<thead>
		<tr class="table-line">
			<th class="table-head">Category<br />ID Products</th>
			<th class="table-head">Product's Name</th>
			<th class="table-head">Price</th>
			<th class="table-head">Sold / Stock</th>
			<th class="table-head">Action</th>
		</tr>
	</thead>
	<tbody>
<?php
if( mysql_num_rows($query) == "0" ){
	echo "<tr class=\"table-line\"><td class=\"table-content\" colspan=\"4\">No offer available. Go create one.</td></tr>";
} else {
	while( $row = mysql_fetch_array($query) ) {
		// Copy data from database to variable(s)
		$idProduk = $row['0'];
		$namaProduk = $row['1'];
		$fotoProduk = $row['2'];
		$harga = $row['3'];
		$stok = $row['4'];
		$kategori = $row['5'];

		$q_pembelian = mysql_query("SELECT STOKPEMBELIAN FROM pembelian WHERE IDPRODUK='$idProduk'");
		$stokPembelian = 0;
		if ( mysql_num_rows( $q_pembelian ) > 0 ) {
			while ( $s_pembelian = mysql_fetch_array( $q_pembelian ) ) {
				$stokPembelian+=$s_pembelian['STOKPEMBELIAN'];
			}
		}

		// Show data with table
		echo "<tr class=\"table-line\">";
		echo "<td class=\"table-content\">" . $kategori . "<br />" . $idProduk     . "</td>";
		echo "<td class=\"table-content\">" . $namaProduk   . "</td>";
		echo "<td class=\"table-content\"> Rp. " . number_format($harga,"2",",",".") . "</td>";
		echo "<td class=\"table-content\">" . $stokPembelian . " / " . $stok;
		echo "<td class=\"table-content\"><a href=\"index.php?mode=edit&idproduk=" . $idProduk . "\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>&nbsp;<a href=\"index.php?mode=delete&idproduk=" . $idProduk . "\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a></td>";
		echo "</tr>";
	}
}
?>
	</tbody>
	<tfoot>
		<tr class="table-line">
			<td colspan="5" class="table-foot"><a href="index.php?mode=add"><span class="glyphicon glyphicon-plus"></span> Add new offer</a></td>
		</tr>
	</tfoot>
</table>