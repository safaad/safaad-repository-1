<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	$idProduk = $_GET['idproduk'];
	$q_produk = mysql_query("SELECT * FROM produk WHERE IDPRODUK = '$idProduk'");
	$s_produk = mysql_fetch_array( $q_produk );
	$idKategori = $s_produk['IDKATEGORI'];
	$q_recommended_produk = mysql_query("SELECT * FROM produk WHERE IDKATEGORI = '$idKategori' AND IDPRODUK != '$idProduk'");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="lib/css/style.css" rel="stylesheet">
		<link href="lib/css/reset.css" rel="stylesheet">

	</head>
	<body>
		<?php getHeader(); ?>
			<div class="container">
				<div id="col">
					<div class="col col-30">
						<div class="col col-sidebar">
							<fieldset>
								<legend align="center">Recommended</legend>
								<?php

								$count = 1;
								while( $s_recommended_produk = mysql_fetch_array( $q_recommended_produk ) ) {
									echo "<a href=\"details.php?idproduk=" . $s_recommended_produk['IDPRODUK'] . "\">";
									echo "<div id=\"col\" class=\"populer\">";
									echo "<div class=\"col col-40\">";
									echo "<div class=\"item-img\">";
									include("function/img.php");
									echo "</div>";
									echo "</div>";
									echo "<div class=\"col col-60\">";
									echo "<div class=\"details\">";
									echo "<span class=\"name\">" . $s_recommended_produk['NAMAPRODUK'] . "</span>";
									echo "<span class=\"price\">Rp. " . number_format( $s_recommended_produk['HARGA'],2,",",".") . "</span>";
									echo "</div>";
									echo "</div>";
									echo "</div>";
									echo "</a>";
									if( $count == 5 ) { break; }
									$count = $count + 1;
								}
								UNSET($s_recommended_produk);
								?>
							</fieldset>
						</div>
					</div>
					<div class="col col-70">
						<div class="col col-content">
							<div class="recommendation buy">
								<div id="col" class="detail">
									<div class="col col-40">
										<div class="img-detail">
											<?php require("function/img.php"); ?>
										</div>
									</div>
									<div class="col col-60 contents">
										<div class="prices">
											<span class="name" align="center"><?php echo $s_produk['NAMAPRODUK']; ?></span><br />
											<table cellpadding="3px">
												<tbody>
													<tr><td>Harga</td><td>:&nbsp;</td><td>Rp. <?php echo number_format( $s_produk['HARGA'],2,",","."); ?></td></tr>
													<tr><td>Stok</td><td>:&nbsp;</td><td><?php echo $s_produk['STOK']; ?> buah</td></tr>
													<?php

													$idToko = $s_produk['IDTOKO'];
													$q_toko = mysql_query("SELECT * FROM toko WHERE IDTOKO = '$idToko'");
													$s_toko = mysql_fetch_array( $q_toko );

													?>
													<tr><td>Toko</td><td>:&nbsp;</td><td><?php echo $s_toko['NAMATOKO']; ?></td></tr>
												</tbody>
											</table>
										</div>
										<div class="footer-detail">
											<button class="btn btn-lg btn-danger btn-beli">Buy</button>
										</div>
									</div>
								</div>
								<div class="keterangan">
									<h5>Detail</h5>
									<div>
									<?php echo $s_produk['DESKRIPSI']; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="lib/js/jquery.min.js"><\/script>')</script>
		<script src="lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="lib/js/custom.js"></script>

	</body>
</html>

<?php
if ( isset( $_POST['submit'] ) ) {
	// Getting data
	$code 			= rand(1,9);
	$idPembelian 	= date( 'YmdHis' ) . $code;
	$idProduk 		= $s_produk['IDPRODUK'];
	$idMember 		= $_SESSION['idMember'];
	$stokPembelian 	= $_POST['stokpembelian'];
	$namaPenerima 	= $_POST['namapenerima'];
	$alamatPenerima = $_POST['alamatpenerima'];
	$noHpPenerima 	= $_POST['nohppenerima'];

	$q_pembelian 	= mysql_query("INSERT INTO pembelian VALUES ('$idPembelian','$idProduk','$idMember','$stokPembelian','$namaPenerima','$alamatPenerima','$noHpPenerima',CURRENT_TIMESTAMP)") or die(mysql_error());
}
?>