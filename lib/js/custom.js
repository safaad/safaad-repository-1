$(document).ready(function() {
	// Display or Hide Sidemenu Section
	$(document).on('click','li#search-button',function() {
		$("ul.circle-navbar").after("<div class=\"sidemenu side-search\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><div class=\"sidemenu-header\"><h2 style=\"color: white\">Search</h2></div><div class=\"sidemenu-body\"><form action=\"/webMagang/category.php\" method=\"POST\"><input type=\"text\" class=\"form-control\" name=\"search-form\"><input type=\"submit\" name=\"submit-search\" class=\"btn btn-search\" value=\"Search\" style=\"margin-top: 3px\"></form></div></div>");
		$("div.side-search").show("fast");
	});
	$(document).on('click','li#offer-button',function() {
		$("ul.circle-navbar").after("<div class=\"sidemenu side-offer\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><div class=\"sidemenu-header\"><h2 style=\"color: white\">Offer</h2></div><div class=\"sidemenu-body\"><form action=\"\" method=\"POST\"><label>Nama Barang</label><input type=\"text\" class=\"form-control\" name=\"stuffName\"><label>Harga</label><input type=\"text\" class=\"form-control\" name=\"stuffPrice\"><label>Stok</label><input type=\"text\" class=\"form-control\" name=\"stuffStock\"><input type=\"submit\" name=\"submit-offer\" class=\"btn btn-offer\" value=\"Create Offer\" style=\"margin-top: 3px\"></form></div></div>");
		$("div.side-offer").show("fast");
	});
	$(document).on('click','li#shopping-button',function() {
		$("ul.circle-navbar").after("<div class=\"sidemenu side-cart\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><div class=\"sidemenu-header\"><h2 style=\"color: white\">Shopping Cart</h2></div><div class=\"sidemenu-body\"><h3>Still in development.</h3> Check back in time.</div></div>");
		$("div.side-cart").show("fast");
	});
	$(document).on('click','li#register-button',function() {
		$("ul.circle-navbar").after("<div class=\"sidemenu side-join\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><div class=\"sidemenu-header\"><h2>Join Us</h2></div><div class=\"sidemenu-body\"><ul><li id=\"sidemenu-login-button\"><span class=\"glyphicon glyphicon-triangle-right\"></span>Login</li><div class=\"sidemenu-login\"><form action=\"\" method=\"POST\"><label>E-Mail</label><input type=\"email\" class=\"form-control\" name=\"email\"><label>Password</label><input type=\"password\" class=\"form-control\" name=\"password\"><input type=\"submit\" name=\"submit-login\" class=\"btn btn-login\" value=\"Login\" style=\"margin-top: 3px\"></form></div><li id=\"sidemenu-register-button\"><span class=\"glyphicon glyphicon-triangle-right\"></span>Register</li><div class=\"sidemenu-register\"><form action=\"\" method=\"POST\" ecntype=\"multipart/form-data\"><label>Full name</label><input type=\"text\" class=\"form-control\" name=\"namemember\"><label>E-Mail</label><input type=\"email\" class=\"form-control\" name=\"email\"><label>Password</label><input type=\"password\" class=\"form-control\" name=\"password\"><label>Phone Number</label><input type=\"text\" class=\"form-control\" name=\"phonenumber\"><input type=\"submit\" name=\"submit-register\" class=\"btn btn-register\" value=\"Register\" style=\"margin-top: 3px\"></form></div></ul></div></div>");
		$("div.side-join").show("fast");
	});
	$(document).on('click','li#config-button',function() {''
		$("ul.circle-navbar").after("<div class=\"sidemenu side-config\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><div class=\"sidemenu-header\"><h2 style=\"color: white\">Option</h2></div><div class=\"sidemenu-body\"><ul><a href=\""/* + window.location.protocol + window.location.hostname*/ + "/webMagang/member/\" class=\"side-option\"><li>Dashboard</li></a><a href=\""/* + window.location.protocol + window.location.hostname*/ + "/webMagang/setting/\" class=\"side-option\"><li>Setting</li></a><a href=\"/webMagang/function/logout.php\" class=\"side-option\"><li>Log out</li></a></ul></div></div>");
		$("div.side-config").show("fast");
	});
	$(document).on('click','li#sidemenu-login-button',function() {
		$("div.sidemenu-register").hide("fast");
		$("div.sidemenu-login").show("fast");
	});
	$(document).on('click','li#sidemenu-register-button',function() {
		$("div.sidemenu-login").hide("fast");
		$("div.sidemenu-register").show("fast");
	});
	$(document).on('click','.glyphicon-remove',function() {
		$("div.sidemenu").remove();
		$("div.footer-detail").show("fast");
		$("div.detail-beli").remove();
	});

	/* BUYING DETAILS */

	$(document).on('click','.btn-beli',function() {
		$("div.keterangan").after("<div class=\"detail-beli\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove\"></span></div><form action=\"\" method=\"POST\"><label for=\"namapenerima\">Nama Penerima</label><br /><input type=\"text\" name=\"namapenerima\" class=\"form-control\" required><br /><label for=\"alamatpenerima\">Alamat Penerima</label><br /><input type=\"text\" name=\"alamatpenerima\" class=\"form-control\"><br /><label for=\"nohppenerima\">No HP Penerima</label><br /><input type=\"text\" name=\"nohppenerima\" class=\"form-control\"><br /><label for=\"stokpembelian\">Stok Pembelian</label><br /><input type=\"number\" name=\"stokpembelian\" class=\"form-control\"><br /><input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"btn btn-default\">&nbsp;<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"btn btn-default\"></form></div>");
		$("div.footer-detail").hide("fast");
		$("div.detail-beli").show("fast");
	});

	/* SETTING UMUM NAME */

	$(document).on('click','span.sunting.names',function(){
		$("span.full-set.names").after("<div class=\"all-ing names\"><form action=\"\" method=\"POST\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove batalan\"></span></div><table class=\"body-set\"><tr><td class=\"depan\">Nama</td><td class=\"belakang\"><input type=\"text\" name=\"name\"></td><td><input type=\"submit\" name=\"submit-name\" value=\"Simpan\" class=\"but-set save\" style=\"padding: 3px 5px\"></td></tr></table></form></div>");

		$("span.sunting.names").hide("fast");
		$("span.sunting.pass").show("fast");
		$("span.sunting.email").show("fast");
		$("span.sunting.no-hp").show("fast");
		$("div.all-ing").show("fast");

		$("div.password").remove();
		$("div.email").remove();
		$("div.no-hp").remove();
	})

	/* SETTING UMUM PASSWORD */

	$(document).on('click','span.sunting.pass',function(){
		$("span.full-set.pass").after("<div class=\"all-ing password\"><form action=\"\" method=\"POST\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove batalan\"></span></div><table class=\"body-set\"><tr><td class=\"depan\">Old Password</td><td class=\"belakang\"><input type=\"text\" name=\"old-pas\"></td></tr><tr><td class=\"depan\">New Password</td><td class=\"belakang\"><input type=\"password\" name=\"new-pas\"></td></tr><tr><td colspan=\"2\"><input type=\"submit\" name=\"submit-pass\" value=\"Simpan\" class=\"but-set save\" style=\"padding: 3px 5px\"></td></tr></table></form></div>");

		$("span.sunting.pass").hide("fast");
		$("span.sunting.names").show("fast");
		$("span.sunting.email").show("fast");
		$("span.sunting.no-hp").show("fast");
		$("div.all-ing").show("fast");

		$("div.names").remove();
		$("div.email").remove();
		$("div.no-hp").remove();
	})

	/* SETTING UMUM EMAIL */

	$(document).on('click','span.sunting.email',function(){
		$("span.full-set.email").after("<div class=\"all-ing email\"><form action=\"\" method=\"POST\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove batalan\"></span></div><table class=\"body-set\"><tr><td class=\"depan\">Old Email</td><td class=\"belakang\"><input type=\"text\" name=\"old-mail\"></td></tr><tr><td class=\"depan\">New Email</td><td class=\"belakang\"><input type=\"email\" name=\"new-mail\"></td></tr><tr><td colspan=\"2\"><input type=\"submit\" name=\"submit-mail\" value=\"Simpan\" class=\"but-set save\" style=\"padding: 3px 5px\"></td></tr></table></form></div>");

		$("span.sunting.email").hide("fast");
		$("span.sunting.names").show("fast");
		$("span.sunting.pass").show("fast");
		$("span.sunting.no-hp").show("fast");
		$("div.all-ing").show("fast");

		$("div.names").remove();
		$("div.password").remove();
		$("div.no-hp").remove();
	})

	/* SETTING UMUM NO-TELP */

	$(document).on('click','span.sunting.no-hp',function(){
		$("span.full-set.no-hp").after("<div class=\"all-ing no-hp\"><form action=\"\" method=\"POST\"><div class=\"right\"><span class=\"glyphicon glyphicon-remove batalan\"></span></div><table class=\"body-set\"><tr><td class=\"depan\">Old No-Telp</td><td class=\"belakang\"><input type=\"text\" name=\"telp\"></td></tr><tr><td colspan=\"2\"><input type=\"submit\" name=\"submit-telp\" value=\"Simpan\" class=\"but-set save\" style=\"padding: 3px 5px\"></td></tr></table></form></div>");

		$("span.sunting.no-hp").hide("fast");
		$("span.sunting.email").show("fast");
		$("span.sunting.names").show("fast");
		$("span.sunting.pass").show("fast");
		$("div.all-ing").show("fast");

		$("div.names").remove();
		$("div.password").remove();
		$("div.email").remove();
	})

	
	/* SETTING TRANSAKSI */

	$(document).on('click','li.riwayat',function(){
		$("div.all-setting").after("<div class=\"transaksi six\"><div class=\"header-setting\"><h4>Riwayat Transaksi</h4></div><table class=\"ri-tran\"><tr><th>No Pembelian</th><th>Nama Barang</th><th>Jumlah</th><th>Harga Satuan</th><th>Sub Total</th><th>Opsi</th></tr><tr><td class=\"ft\">1</td><td class=\"ft\">Kaos Distro</td><td class=\"ft\">4</td><td class=\"ft\">80.000</td><td class=\"ft\">320.000</td><td class=\"ft\"><button class=\"but-set\">Hapus</button></td></tr></table></div>");
	})

	$(document).on('click','.batalan',function(){
		$("div.all-ing").remove();
		$("span.names").show("fast");
		$("span.sunting.names").show("fast");
		$("span.sunting.pass").show("fast");
		$("span.sunting.email").show("fast");
		$("span.sunting.no-hp").show("fast");
	})
	$(document).on('click','div.alert',function(){
		$(this).hide();
		$(this).remove();
	});
});