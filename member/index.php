<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	if( !isset( $_SESSION['login'] ) or $_SESSION['login'] == false ) {
		header("location:../login.php");
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="../lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="../lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="../lib/css/style.css" rel="stylesheet">
		<link href="../lib/css/reset.css" rel="stylesheet">
		<link href="../lib/css/stylecadangan.css" rel="stylesheet">

	</head>
	<body>
		<?php getHeader(); ?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<?php 
								$idMember = $_SESSION['idMember'];
								$q_name = mysql_query("SELECT NAMAMEMBER FROM member WHERE IDMEMBER='$idMember'");
								$s_name = mysql_fetch_array( $q_name );
							?>
							<legend><?php echo $s_name['NAMAMEMBER']; ?></legend>
							<ul class="sidebar user-sidebar">
								<a href="#"><li class="border-neutral active">Home</li></a>
								<?php if( $_SESSION['position'] == "1" ) { ?>
								<a href="../admin/"><li class="border-neutral">Admin panel</li></a>
								<?php } ?>
								<a href="profile/"><li class="border-neutral">Profile</li></a>
								<a href="olshop"><li class="border-neutral">Your online shop</li></a>
								<a href="cart"><li class="border-neutral">Shopping cart</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../lib/js/custom.js"></script>

	</body>
</html>