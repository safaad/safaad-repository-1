<?php
if( isset( $_SESSION['idMember'] ) ) {
	if( isset( $_GET['mode'] ) ) {
		if( $_GET['mode'] == "add") {
			?>
			<form action="../function/olshop/add.php" method="POST" style="padding: 15px">
				<label for="idToko">ID Toko</label><br/>
				<input class="form-control" type="text" name="idToko" value="Auto generate" readonly><br/>
				<label for="namaToko">Nama Toko</label><br/>
				<input class="form-control" type="text" name="namaToko" required><br/>
				<label for="alamatToko">Alamat Toko</label><br/>
				<textarea class="form-control" name="alamatToko" required></textarea><br/>
				<input class="btn btn-default" type="submit" name="submit" value="Submit">&nbsp;<input class="btn btn-default" type="reset" name="reset" value"Reset">
			</form>
			<?php
		} else {?>
			<script>alert(\"You are not in add mode. We will redirecting you to last page.");</script>
			<meta http-equiv='refresh' content='0;url=index.php'>
		<?php
		}
	} else {
		echo "<script>alert(\"Sorry, the data is currently unavailable. Please try again in few moment.\");";
		echo "<meta http-equiv='refresh' content='0;url=index.php'>";
	}
} else {
?>

<script>alert("Sorry, we can't proceed your account. Please retry login.");</script>
<meta http-equiv="refresh" content="0;url=index.php">

<?php	
}

?>