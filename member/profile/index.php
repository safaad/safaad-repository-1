<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	$uId = $_SESSION['idMember'];

	$query = mysql_query("SELECT * FROM member WHERE IDMEMBER = '$uId'");
	if ( $row = mysql_fetch_array($query) ) {
		$name=$row['2'];
		$email=$row['3'];
		$noHp=$row['4'];
	}

	$queryy = mysql_query("SELECT * FROM profil WHERE IDMEMBER = '$uId'");
	$count_queryy = mysql_num_rows($queryy);
	if ( $count_queryy == 0 ) {
		$tgl = "null";
		$bln = "null";
		$thn = "null";
		$jk = "null";
		$agama = "null";
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="../../lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="../../lib/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-social.css">
		
		<!-- Custom CSS -->
		<link href="../../lib/css/style.css" rel="stylesheet">
		<link href="../../lib/css/reset.css" rel="stylesheet">
		<link href="../../lib/css/stylecadangan.css" rel="stylesheet">

	</head>
	<body>
		<?php getHeader(); 
		if(!isset($_SESSION['login'])) header("location:../login/");
		?>
		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<legend>Member Name</legend>
							<ul class="sidebar user-sidebar">
								<a href=".."><li class="border-neutral">Home</li></a>
								<a href="../../admin/"><li class="border-neutral">Admin panel</li></a>
								<a href="./profile/"><li class="border-neutral active">Profile</li></a>
								<a href="../olshop/"><li class="border-neutral">Your online shop</li></a>
								<a href="../cart"><li class="border-neutral">Shopping cart</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
						<div class="col col-30" align="center">
							<?php
							$uId = $_SESSION['idMember'];
								$q_member = mysql_query("SELECT * FROM member WHERE IDMEMBER = '$uId'");
								while( $s_member = mysql_fetch_array( $q_member )) {
									echo "<div class=\"gambar\" align=\"center\">";
									require("../../function/img.php");
									echo "</div>";
								}
							?>
						</div>
						<div class="col col-70">
							<div class="set-profil">
								<div class="body-profil">
									<div>
										<div class="isi-profil">
											<div class="judul">
												<span style="text-transform: uppercase">informasi kontak</span>
											</div>
											<ul class="ulList-profil" style="padding-bottom: 30px">
												<li class="liList-profil no">
													<div class="ful">
														<div class="left-profil">
															<span class="left-conten"><b>Nama</b></span>
														</div>
														<div class="right-profil">
															<span class="right-conten"><?php echo $name; ?> </span>
														</div>
													</div>
												</li>
												<li class="liList-profil no">
													<div class="ful">
														<div class="left-profil">
															<span class="left-conten"><b>Email</b></span>
														</div>
														<div class="right-profil">
															<span class="right-conten"><?php echo $email; ?> </span>
														</div>
													</div>
												</li>
												<li class="liList-profil no">
													<div class="ful">
														<div class="left-profil">
															<span class="left-conten"><b>Telepon Seluler</b></span>
														</div>
														<div class="right-profil">
															<span class="right-conten"><?php echo $noHp; ?></span>
														</div>
													</div>
												</li>
											</ul>
											<?php
												if ( isset ( $_GET['mode'] ) ) {
													if ($_GET['mode']=="edit") {
														require_once("edit.php");
													}else if ($_GET['mode']=="add" ) {
														require_once("add.php");
													}
												} else {
													require_once("../function/profile/list.php");
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>

		<!-- Bootstrap core JavaScript -->
		<!-- =============================================================== -->
		<script scr="../../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../../lib/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../../lib/js/custom.js"></script>
		<script src="../../lib/js/buy.js"></script>

	</body>
</html>