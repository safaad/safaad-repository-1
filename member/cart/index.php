<?php

	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php");

	if( !isset( $_SESSION['login'] ) or $_SESSION['login'] == false ) {
		header("location:" . $_SERVER['DOCUMENT_ROOT'] . "/webMagang/login.php");
	}

?>

<!DOCTYPE html>
	<html>
		<head>

			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<meta name="description" content="">
			<meta name="author" content="Safaad">

			<title>Shopping Cart | Safaad</title>

			<!-- Bootstrap core CSS -->
			<link href="../../lib/css/bootstrap.css" rel="stylesheet">

			<!-- Things for Social Button -->
			<link href="../../lib/css/font-awesome.css" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="../../lib/css/bootstrap-social.css">

			<!-- Custom CSS -->
			<link href="../../lib/css/style.css" rel="stylesheet">
			<link href="../../lib/css/reset.css" rel="stylesheet">
			<link href="../../lib/css/stylecadangan.css" rel="stylesheet">

		</head>
		<body>
		<?php getHeader(); ?>

		<div class="container">
			<div id="col">
				<div class="col col-30">
					<div class="col col-sidebar">
						<fieldset>
							<?php 
								$idMember = $_SESSION['idMember'];
								$q_name = mysql_query("SELECT NAMAMEMBER FROM member WHERE IDMEMBER='$idMember'");
								$s_name = mysql_fetch_array( $q_name );
							?>
							<legend><?php echo $s_name['NAMAMEMBER']; ?></legend>
							<ul class="sidebar user-sidebar">
								<a href="../"><li class="border-neutral">Home</li></a>
								<?php if( $_SESSION['position'] == "1" ) { ?>
								<a href="../../admin/"><li class="border-neutral">Admin panel</li></a>
								<?php } ?>
								<a href="../profile/"><li class="border-neutral">Profile</li></a>
								<a href="../olshop"><li class="border-neutral">Your online shop</li></a>
								<a href="./"><li class="border-neutral active">Shopping cart</li></a>
							</ul>
						</fieldset>
					</div>
				</div>
				<div class="col col-70">
					<div class="col col-content">
						<h4 class="content-table-head">Manage shopping cart</h4>
						<p class="content-table-description">Manage product you bought</p>
						<div class="seperator"></div>
						<?php
						if( isset( $_GET['alert'] ) ) {
							if( $_GET['alert'] == "editfailed" ) {
								echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to update data, please try again in few moment. Click on me to dismiss.</div>";
							} else if ( $_GET['alert'] == "editsuccess" ) {
								echo "<div class=\"alert alert-success\" role=\"alert\"><strong> Succesfully Updated!</strong> Your data succesfully updated. Click on me to dismiss.</div>";
							} else if ( $_GET['alert'] == "addfailed" ) {
								echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to add data, please try again in few moment. Click on me to dismiss.</div>";
							} else if ( $_GET['alert'] == "addsuccess" ) {
								echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Succesfully Added!</strong> Your data successfully added. Click on me to dismiss.</div>";
							} else if ( $_GET['alert'] == "deletefailed" ) {
								echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> Failed to delete data, please try again in few moment. Click on  me to dismiss.</div>";
							} else if ( $_GET['alert'] == "deletesuccess" ) {
								echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Successfully Deleted!</strong> Your data successfully deleted. Click on me to dismiss.</div>";
							}
						}
						if ( isset( $_GET['mode'] ) ) {
							if ( $_GET['mode'] == "add" ){
								require_once("add.php");
							} else if ( $_GET['mode'] == "edit" ) {
								require_once("edit.php");
							} else if ( $_GET['mode'] == "delete" ) {
								require_once("../function/cart/delete.php");
							}
						} else {
							require_once("../function/cart/list.php");
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php getFooter(); ?>
		<!-- Boostrap core JavaScript -->
		<!-- =========================================== -->
		<script scr="../../lib/js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../lib/js/jquery.min.js"><\/script>')</script>
		<script src="../../lib/js/boostrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Window 8 bug -->
		<script src="../../lib/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../../lib/js/custom.js"></script>
		<script src="../../lib/js/buy.js"></script>
	</body>
</html>