<?php

session_start();
	unset($_SESSION);
	session_destroy();
	// Use a line below when logging out redirect to index
	echo "<meta http-equiv=\"refresh\" content=0;url='../index.php'>";

	// Use these line below when logging out redirect to last page 
  	// <script>history.back();</script>

?>