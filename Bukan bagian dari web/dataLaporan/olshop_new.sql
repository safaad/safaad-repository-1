-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 24. April 2016 jam 06:23
-- Versi Server: 5.5.8
-- Versi PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `olshop`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `IDKATEGORI` varchar(15) NOT NULL,
  `KATEGORINAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`IDKATEGORI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`IDKATEGORI`, `KATEGORINAME`) VALUES
('1', 'Aksesoris & Fashion'),
('2', 'Elektronik'),
('3', 'Hobi'),
('4', 'Office &Stationery'),
('5', 'Olahraga'),
('6', 'Otomotif'),
('7', 'Rumah Tangga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `IDMEMBER` varchar(15) NOT NULL,
  `IDPOSITION` varchar(15) DEFAULT NULL,
  `NAMAMEMBER` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `NOHP` varchar(14) DEFAULT NULL,
  `KATASANDI` varchar(32) DEFAULT NULL,
  `FOTOPROFIL` longblob,
  `FORMATFOTOPROFIL` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`IDMEMBER`),
  KEY `FK_RELATIONSHIP_11` (`IDPOSITION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`IDMEMBER`, `IDPOSITION`, `NAMAMEMBER`, `EMAIL`, `NOHP`, `KATASANDI`, `FOTOPROFIL`, `FORMATFOTOPROFIL`) VALUES
('0', '1', 'Admin', 'admin@localhost', '0968368918322', '21232f297a57a5a743894a0e4a801fc3', '', ''),
('201604232155329', '3', 'kaka', 'kaka@gmail', '0987654321101', '5541c7b5a06c39b267a5efae6628e003', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `IDPEMBELIAN` varchar(15) NOT NULL,
  `IDPRODUK` varchar(15) DEFAULT NULL,
  `IDMEMBER` varchar(15) DEFAULT NULL,
  `STOKPEMBELIAN` int(11) DEFAULT NULL,
  `NAMAPENERIMA` varchar(30) DEFAULT NULL,
  `ALAMATPENERIMA` varchar(50) DEFAULT NULL,
  `NOHPPENERIMA` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`IDPEMBELIAN`),
  KEY `FK_RELATIONSHIP_3` (`IDPRODUK`),
  KEY `FK_RELATIONSHIP_4` (`IDMEMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `IDPOSITION` varchar(15) NOT NULL,
  `POSITIONNAME` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IDPOSITION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `position`
--

INSERT INTO `position` (`IDPOSITION`, `POSITIONNAME`) VALUES
('1', 'Superadmin'),
('2', 'Admin'),
('3', 'Member'),
('4', 'Banned');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `IDPRODUK` varchar(15) NOT NULL,
  `IDKATEGORI` varchar(15) DEFAULT NULL,
  `IDTOKO` varchar(15) DEFAULT NULL,
  `NAMAPRODUK` varchar(30) DEFAULT NULL,
  `FOTOPRODUK` longblob,
  `FORMATFOTOPRODUK` varchar(5) DEFAULT NULL,
  `HARGA` float DEFAULT NULL,
  `STOK` int(11) DEFAULT NULL,
  `DESKRIPSI` text,
  PRIMARY KEY (`IDPRODUK`),
  KEY `FK_RELATIONSHIP_10` (`IDKATEGORI`),
  KEY `FK_RELATIONSHIP_2` (`IDTOKO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`IDPRODUK`, `IDKATEGORI`, `IDTOKO`, `NAMAPRODUK`, `FOTOPRODUK`, `FORMATFOTOPRODUK`, `HARGA`, `STOK`, `DESKRIPSI`) VALUES
('201604201101314', '7', '201604151310522', 'Shampo', NULL, NULL, 15000, 2, 'sampo untuk rambut kepala');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
  `IDMEMBER` varchar(15) NOT NULL,
  `TANGGAL` int(5) NOT NULL,
  `BULAN` varchar(10) NOT NULL,
  `TAHUN` int(5) NOT NULL,
  `JK` varchar(20) NOT NULL,
  `AGAMA` varchar(20) NOT NULL,
  KEY `IDMEMBER` (`IDMEMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil`
--

INSERT INTO `profil` (`IDMEMBER`, `TANGGAL`, `BULAN`, `TAHUN`, `JK`, `AGAMA`) VALUES
('0', 10, 'April', 1990, 'Laki-Laki', 'Islam'),
('201604232155329', 25, 'April', 1995, 'Perempuan', 'Islam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `IDREVIEW` varchar(15) NOT NULL,
  `IDMEMBER` varchar(15) DEFAULT NULL,
  `IDPRODUK` varchar(15) DEFAULT NULL,
  `REVIEWCONTENT` text,
  `REVIEWLIKE` int(11) DEFAULT NULL,
  `REVIEWDISLIKE` int(11) DEFAULT NULL,
  `RATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDREVIEW`),
  KEY `FK_RELATIONSHIP_6` (`IDPRODUK`),
  KEY `FK_RELATIONSHIP_9` (`IDMEMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--

CREATE TABLE IF NOT EXISTS `toko` (
  `IDTOKO` varchar(15) NOT NULL,
  `IDMEMBER` varchar(15) DEFAULT NULL,
  `NAMATOKO` varchar(30) DEFAULT NULL,
  `ALAMATTOKO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDTOKO`),
  KEY `FK_RELATIONSHIP_1` (`IDMEMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`IDTOKO`, `IDMEMBER`, `NAMATOKO`, `ALAMATTOKO`) VALUES
('201604151310522', '0', 'tokoadminbaru', 'alamattokoadminbaru');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `IDWISHLIST` varchar(15) NOT NULL,
  `IDMEMBER` varchar(15) DEFAULT NULL,
  `IDPRODUK` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IDWISHLIST`),
  KEY `FK_RELATIONSHIP_5` (`IDPRODUK`),
  KEY `FK_RELATIONSHIP_8` (`IDMEMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `wishlist`
--


--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`IDPOSITION`) REFERENCES `position` (`IDPOSITION`);

--
-- Ketidakleluasaan untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`IDPRODUK`) REFERENCES `produk` (`IDPRODUK`),
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`IDMEMBER`) REFERENCES `member` (`IDMEMBER`);

--
-- Ketidakleluasaan untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`IDKATEGORI`) REFERENCES `kategori` (`IDKATEGORI`),
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`IDTOKO`) REFERENCES `toko` (`IDTOKO`);

--
-- Ketidakleluasaan untuk tabel `profil`
--
ALTER TABLE `profil`
  ADD CONSTRAINT `profil_ibfk_1` FOREIGN KEY (`IDMEMBER`) REFERENCES `member` (`IDMEMBER`);

--
-- Ketidakleluasaan untuk tabel `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`IDPRODUK`) REFERENCES `produk` (`IDPRODUK`),
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`IDMEMBER`) REFERENCES `member` (`IDMEMBER`);

--
-- Ketidakleluasaan untuk tabel `toko`
--
ALTER TABLE `toko`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`IDMEMBER`) REFERENCES `member` (`IDMEMBER`);

--
-- Ketidakleluasaan untuk tabel `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`IDPRODUK`) REFERENCES `produk` (`IDPRODUK`),
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`IDMEMBER`) REFERENCES `member` (`IDMEMBER`);
