/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     13/04/2016 10:31:52                          */
/*==============================================================*/


drop table if exists KATEGORI;

drop table if exists MEMBER;

drop table if exists PEMBELIAN;

drop table if exists POSITION;

drop table if exists PRODUK;

drop table if exists REVIEW;

drop table if exists TOKO;

drop table if exists WISHLIST;

/*==============================================================*/
/* Table: KATEGORI                                              */
/*==============================================================*/
create table KATEGORI
(
   IDKATEGORI           varchar(15) not null,
   KATEGORINAME         varchar(30),
   primary key (IDKATEGORI)
);

/*==============================================================*/
/* Table: MEMBER                                                */
/*==============================================================*/
create table MEMBER
(
   IDMEMBER             varchar(15) not null,
   IDPOSITION           varchar(15),
   NAMAMEMBER           varchar(30),
   EMAIL                varchar(50),
   NOHP                 varchar(14),
   KATASANDI            varchar(32),
   FOTOPROFIL           longblob,
   FORMATFOTOPROFIL     varchar(5),
   primary key (IDMEMBER)
);
insert into MEMBER values
(
   '0',
   '1',
   'Admin',
   'admin@localhost',
   '',
   '21232f297a57a5a743894a0e4a801fc3',
   '',
   ''
);

/*==============================================================*/
/* Table: PEMBELIAN                                             */
/*==============================================================*/
create table PEMBELIAN
(
   IDPEMBELIAN          varchar(15) not null,
   IDPRODUK             varchar(15),
   IDMEMBER             varchar(15),
   STOKPEMBELIAN        int,
   NAMAPENERIMA         varchar(30),
   ALAMATPENERIMA       varchar(50),
   NOHPPENERIMA         varchar(14),
   primary key (IDPEMBELIAN)
);

/*==============================================================*/
/* Table: POSITION                                              */
/*==============================================================*/
create table POSITION
(
   IDPOSITION           varchar(15) not null,
   POSITIONNAME         varchar(15),
   primary key (IDPOSITION)
);
insert into POSITION values
(
   '1','Superadmin'
),
(
   '2','Admin'
),
(
   '3','Member'
),
(
   '4','Banned'
);

/*==============================================================*/
/* Table: PRODUK                                                */
/*==============================================================*/
create table PRODUK
(
   IDPRODUK             varchar(15) not null,
   IDKATEGORI           varchar(15),
   IDTOKO               varchar(15),
   NAMAPRODUK           varchar(30),
   FOTOPRODUK           longblob,
   FORMATFOTOPRODUK     varchar(5),
   HARGA                float(8),
   STOK                 int,
   DESKRIPSI            text,
   primary key (IDPRODUK)
);

/*==============================================================*/
/* Table: REVIEW                                                */
/*==============================================================*/
create table REVIEW
(
   IDREVIEW             varchar(15) not null,
   IDMEMBER             varchar(15),
   IDPRODUK             varchar(15),
   REVIEWCONTENT        text,
   REVIEWLIKE           int,
   REVIEWDISLIKE        int,
   RATE                 int,
   primary key (IDREVIEW)
);

/*==============================================================*/
/* Table: TOKO                                                  */
/*==============================================================*/
create table TOKO
(
   IDTOKO               varchar(15) not null,
   IDMEMBER             varchar(15),
   NAMATOKO             varchar(30),
   ALAMATTOKO           varchar(255),
   primary key (IDTOKO)
);

/*==============================================================*/
/* Table: WISHLIST                                              */
/*==============================================================*/
create table WISHLIST
(
   IDWISHLIST           varchar(15) not null,
   IDMEMBER             varchar(15),
   IDPRODUK             varchar(15),
   primary key (IDWISHLIST)
);

alter table MEMBER add constraint FK_RELATIONSHIP_11 foreign key (IDPOSITION)
      references POSITION (IDPOSITION) on delete restrict on update restrict;

alter table PEMBELIAN add constraint FK_RELATIONSHIP_3 foreign key (IDPRODUK)
      references PRODUK (IDPRODUK) on delete restrict on update restrict;

alter table PEMBELIAN add constraint FK_RELATIONSHIP_4 foreign key (IDMEMBER)
      references MEMBER (IDMEMBER) on delete restrict on update restrict;

alter table PRODUK add constraint FK_RELATIONSHIP_10 foreign key (IDKATEGORI)
      references KATEGORI (IDKATEGORI) on delete restrict on update restrict;

alter table PRODUK add constraint FK_RELATIONSHIP_2 foreign key (IDTOKO)
      references TOKO (IDTOKO) on delete restrict on update restrict;

alter table REVIEW add constraint FK_RELATIONSHIP_6 foreign key (IDPRODUK)
      references PRODUK (IDPRODUK) on delete restrict on update restrict;

alter table REVIEW add constraint FK_RELATIONSHIP_9 foreign key (IDMEMBER)
      references MEMBER (IDMEMBER) on delete restrict on update restrict;

alter table TOKO add constraint FK_RELATIONSHIP_1 foreign key (IDMEMBER)
      references MEMBER (IDMEMBER) on delete restrict on update restrict;

alter table WISHLIST add constraint FK_RELATIONSHIP_5 foreign key (IDPRODUK)
      references PRODUK (IDPRODUK) on delete restrict on update restrict;

alter table WISHLIST add constraint FK_RELATIONSHIP_8 foreign key (IDMEMBER)
      references MEMBER (IDMEMBER) on delete restrict on update restrict;

