<!-- IT'S A SPECIAL PAGE -->
<?php

require_once("function/router.php");
require_once("function/function.php");

?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<meta name="description" content="">
			<meta name="author" content="Safaad">

			<title>Register | Safaad</title>

			<!-- Bootstrap core CSS -->
			<link rel="stylesheet" href="lib/css/bootstrap.css">
			<!-- Thing for Social Button ( Font Awesome ) -->
			<link rel="stylesheet" href="lib/css/font-awesome.css">
			<!-- Custom CSS -->
			<link rel="stylesheet" href="lib/css/style.css">
			<link rel="stylesheet" href="lib/css/stylecadangan.css">
			<link rel="stylesheet" href="lib/css/reset.css">
		</head>
		<body>
			<header class="navbarheader">
				<div class="container">
					<div id="col">
						<div class="col col-20">
							<a href="./"><img class="logo" src="lib/img/logo-backend1.png"></a>
						</div>
						<div class="col col-80">&nbsp;</div>
					</div>
				</div>
			</header>
			<div class="container" style="margin-top: 5px; background-color: white; border: 1px solid gray;">
				<div class="row" style="padding: 25px;">
					<div class="col-md-8">
					<h2>Join Us</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrus exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="separator"></div>
					<h3>Supported payment methods</h3>
					<div class="row">
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_mega.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_bni.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_bri.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_btn.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_muamalat.jpg" style="width: 100%; height: auto">
						</div>
						<div class="col-md-2" style="padding: 0">
						<img src="lib/img/bank_mandiri.jpg" style="width: 100%; height: auto">
						</div>
					</div>
					</div>
					<div class="col-md-4">
					<h2>Register</h2>
					<?php

					if ( isset( $_GET['alert'] ) ) {
						if ( $_GET['alert'] == "password_not_same" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Password doesn't same!</strong> Please re-type your password. Click to dissmiss.</div>";
						} else if ( $_GET['alert'] == "email_already_exist" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>E-mail already exist!</strong> We have user that already use this email, please re-type your e-mail. Click to dissmiss.</div>";
						} else if ( $_GET['alert'] == "register_failed" ) {
							echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Oops!</strong> We have issues on our server please try again later.</div>";
						} else if ( $_GET['alert'] == "register_success" ) {
							echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Congratulations!</strong> Register successfully. Please login to continue.</div>";
						}
					}
					if ( !isset( $_GET['idMember'] ) ) {
					?>
					<form action="" method="POST">
						<label for="fullname">Full Name</label><br />
						<input class="form-control" type="text" name="fullname" required ><br />
						<label for="email">E-Mail</label><br />
						<input class="form-control" type="email" name="email" required><br />
						<label for="password">Password</label><br />
						<input class="form-control" type="password" name="password" required><br />
						<label for="repassword">Re-Type Password</label><br />
						<input class="form-control" type="password" name="repassword" required><br />
						<label for="phonenumber">Phone Number</label><br />
						<input class="form-control" type="text" name="phonenumber" required><br />
						<input type="checkbox" name="eula-agree" value="agree"/>&nbsp;I agree with <a href="#">Safaad's User License and Agreements</a> and i will help to improve the development of Safaad<br /><br />
						<input class="btn btn-default" type="submit" name="submit" value="Register">&nbsp;&nbsp;<a href="#">Already have an account?</a>
					</form>
					<?php
						} else {
					?>
					<form action="" method="POST" enctype="multipart/form-data">
						<label for="fotoprofil">Add Foto Profil</label>
						<input type="file" name="fotoprofil"><br />
						<input type="submit" value="Upload" name="upload">
					</form>
					<?php
						}
					?>
					</div>
				</div>
			</div>
			<footer>
				<div class="container">
					<div id="col">
						<div class="col col-50">
							@Copyright 2016. All Right Reserved. 
						</div>
						<div class="col col-50">
						</div>
					</div>
				</div>
			</footer>
			<!-- Bootstrap Core JS -->
			<script src="lib/js/jquery.min.js"></script>
			<script>window.jQuery || document.write('<script src="lib/js/jquery.min.js"><\/script>')</script>
			<script src="lib/js/bootstrap.js"></script>
			<script src="lib/js/ie10-viewport-bug-workaround.js"></script>
			<script src="lib/js/custom.js"></script>
			<script src="lib/js/buy.js"></script>
		</body>
	</html>

<?php
if( isset( $_POST['upload'] ) ) {
	// Getting image data from form
	$file_name = $_FILES['fotoprofil']['name'];
	$tmp_name = $_FILES['fotoprofil']['tmp_name'];
	$file_size = $_FILES['fotoprofil']['size'];
	$file_type = $_FILES['fotoprofil']['type'];
	echo $file_name;
	// Proccessing image data
	$fp = fopen($tmp_name,"r");
	$file_content = fread($fp,$file_size);
	$file_content = mysql_real_escape_string($file_content);
	fclose($fp);

	$idMember = $_GET['idMember'];
	$q_register = mysql_query("UPDATE member SET FOTOPROFIL='$file_content' WHERE IDMEMBER='$idMember'") or die(mysql_error());
	if ( $q_register ) {
		echo "<meta http-equiv='refresh' content='0;url=register.php?alert=register_success'>";
	} else echo "<meta http-equiv='refresh' content='10;url=register.php?alert=register_failed'>";
}
if( isset( $_POST['submit'] ) ) {
	// Getting data from form
	$fullname		= $_POST['fullname'];
	$email   		= $_POST['email'];
	$password		= md5($_POST['password']);
	$repassword		= md5($_POST['repassword']);
	$phonenumber	= $_POST['phonenumber'];

	// Getting error message
	// Password doesn't same
	if ( $password != $repassword ) {
		echo "<meta http-equiv='refresh' content='0;url=register.php?alert=password_not_same'>";
	} else {
		// E-Mail already exist
		$q_email = mysql_query("SELECT * FROM member WHERE EMAIL='$email'");
		$c_email = mysql_num_rows( $q_email );
		if( $c_email   != 0 ) {
			echo "<meta http-equiv='refresh' content='0;url=register.php?alert=email_already_exist'>";
		} else {
			// Entry data to database
			$code 	= rand(1,9);
			$iduser	= date( 'YmdHis' ) . $code;
			$q_register = mysql_query("INSERT INTO member VALUES('$iduser','3','$fullname','$email','$phonenumber','$password',null,null)");

			// Error
			if ( !$q_register ) {
				echo "<meta http-equiv='refresh' content='0;url=register.php?idMember=" . $iduser . "'>";
			} else echo "<meta http-equiv='refresh' content='0;url=register.php?idMember=" . $iduser . "'>";
		}
	}
}

?>