<!DOCTYPE html>
	<html>
			<?php 
			require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/router.php" );
			require_once($_SERVER['DOCUMENT_ROOT'] . "/webMagang/function/function.php" );
			?>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="Safaad">

		<title>Contoh Customisasi Website</title>

		<!-- Bootstrap core CSS -->
		<link href="lib/css/bootstrap.css" rel="stylesheet">

		<!-- Things for Social Button -->
		<link href="lib/css/font-awesome.css" rel="stylesheet">
		
		<!-- Custom CSS -->
		<link href="lib/css/style.css" rel="stylesheet">
		<link href="lib/css/reset.css" rel="stylesheet">
		</head>
		<body>
				<?php getHeader(); ?>
				<div class="container">
					<div id="col">
						<div class="col col-30">
							<div class="col col-sidebar">
								<fieldset>
									<legend align="center">Kategori</legend>
									<ul class="sidebar">
										<a href="category.php?idkategori=1"><li class="border-aksesoris-fashion">Aksesoris & Fashion</li></a>
										<a href="category.php?idkategori=2"><li class="border-elektronik">Elektronik</li></a>
										<a href="category.php?idkategori=3"><li class="border-hobi">Hobi</li></a>
										<a href="category.php?idkategori=4"><li class="border-office-stationery">Office & Stationery</li></a>
										<a href="category.php?idkategori=5"><li class="border-olahraga">Olahraga</li></a>
										<a href="category.php?idkategori=6"><li class="border-otomotif">Otomotif</li></a>
										<a href="category.php?idkategori=7"><li class="border-rumahtangga">Rumah Tangga</li></a>
									</ul>
								</fieldset>
							</div>
						</div>
						<div class="col col-70">
							<div class="col col-content">
							<?php
								$q_kategori = mysql_query("SELECT * FROM kategori");
								while ( $s_kategori = mysql_fetch_array( $q_kategori ) ) {
									$idKategori = $s_kategori['0'];
									$q_produk = mysql_query("SELECT * FROM produk WHERE IDKATEGORI = '$idKategori' ORDER BY IDPRODUK");
									if ( mysql_num_rows( $q_produk ) != 0) {
							?>
								<div class="recommendation">
									<fieldset>
										<legend class="recommendation-title"><?php echo $s_kategori['1']; ?> <a href="category.php?idkategori=<?php echo $s_kategori['0']; ?>"><span class="see-all">See all</span></a></legend>
										<div id="col">
											<?php
											$count=1;
											while( $s_produk = mysql_fetch_array( $q_produk ) ) {
												echo "<div class=\"col col-25\" style=\"padding: 3px\">";
												echo "<a href=\"details.php?idproduk=" . $s_produk['IDPRODUK'] . "\">";
												echo "<div class=\"recommendation-image\">";
												require("function/img.php");
												echo "</div>";
												echo "<div class=\"recommendation-detail\">";
												echo "<span class=\"name\">" . $s_produk['NAMAPRODUK'] . "</span>";
												echo "<span class=\"price\">Rp. " . number_format( $s_produk['HARGA'],2,",",".") . "</span>";
												echo "</div>";
												echo "</a>";
												echo "</div>";
												if( $count == 4 ) {break;}
												$count = $count + 1;
											}
											?>
										</div>
									</fieldset>
								</div>
							<?php
									}
								}
							?>
							</div>
						</div>
					</div>
				</div>
				<?php getFooter(); ?>

			<!-- Bootstrap core JavaScript -->
			<!-- =============================================================== -->
			<script scr="lib/js/jquery.min.js"></script>
			<script>window.jQuery || document.write('<script src="lib/js/jquery.min.js"><\/script>')</script>
			<script src="lib/js/bootstrap.min.js"></script>
			<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
			<script src="lib/js/ie10-viewport-bug-workaround.js"></script>
			<script src="lib/js/custom.js"></script>
		</body>
	</html>

	